```
# DEMO Config

# DEMO
kubectl vsphere version
kubectl vsphere login --server=tanzuk8s.dev.gov.sg -u adminkyh_dc@dev.gov.sg --insecure-skip-tls-verify=true --tanzu-kubernetes-cluster-namespace platform --tanzu-kubernetes-cluster-name publishing-platform
kubectl config get-contexts
kubectl config use-context publishing-platform
kubectl -n publishing-platform get pods 

cd kube
kubectl apply -f .\setup\
kubectl -n publishing-platform get pods 
kubectl apply -f .\app

http://publishing.outpost.ws:8080/search/
http://publishing.outpost.ws/

# Login supervisor
kubectl vsphere version
kubectl vsphere login --server=tanzuk8s.dev.gov.sg -u adminkyh_dc@dev.gov.sg --insecure-skip-tls-verify=true --tanzu-kubernetes-cluster-namespace platform --tanzu-kubernetes-cluster-name publishing-platform

# Check on cluster
kubectl config get-contexts
kubectl config use-context platform
kubectl get tkc
kubectl describe tkc publishing-platform

kubectl config use-context publishing-platform
```

automation on private cloud
constantly evolving, relevant services are in the pipeline
update legend on 10 svc icons remove layout